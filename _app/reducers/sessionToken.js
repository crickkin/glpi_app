/*Este Reducer tem a função de armazenar ou remover o token de acesso do redux*/
import * as c from '../common/constants';

export const sessionToken = (state = [], action) => {
    switch(action.type)
    {
        case c.GET_SESSION_TOKEN: 
            let sessionToken = action.sessionToken;
            return {
                ...state, 
                sessionToken
            }
        case c.REMOVE_SESSION_TOKEN:
            sessionToken = '';
            return {
                ...state,
                sessionToken
            }
    }
    return state;
}