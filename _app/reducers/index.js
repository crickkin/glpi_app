/*Combinção de reducers para serem importados para o redux*/
import { combineReducers } from 'redux'

import {sessionToken} from './sessionToken'

export default combineReducers({
  sessionToken,
})
