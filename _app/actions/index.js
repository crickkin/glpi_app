//_app/actions/index.js
import * as c from '../common/constants';
import base64 from 'react-native-base64';

/*Login e senha são transformados em base64 para autênticação na api e caso validados, 
utilizam um callback para tratar a resposta na aplicação*/
export function initSession(login, password, callback){
    return (dispatch) => {
        var text = login + ':' + password;
        const authLogin = base64.encode(text);

        const url = c.API_REST + 'initSession/';

        fetch(url, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Basic ' + authLogin
            }
        })
        .then(response => response.json())
        .then(s => {
            callback(s.session_token);  //O callback permite que a resposta da api seja tratada na página onde foi feita a chamada
            dispatch({ type: c.GET_SESSION_TOKEN, sessionToken: s.session_token});  //O dispatch salva o session token no redux
        })
        .catch(err => {
            alert(err);
        });
    }
};