/*O Store serve para criar o local de armazenamento para o redux, 
que será compartilhado pelos componentes da api*/
import { 
    createStore,
    applyMiddleware
} from 'redux';
import thunk from 'redux-thunk';
import reducers from '../reducers';

export const store = createStore(reducers, applyMiddleware(thunk));