/*Este componente tem a função de criar uma lista de itens para ser mostrado na tela*/
import React, { Component } from 'react';
import { 
    View,
    Text,
    FlatList,
    TouchableOpacity,
    StyleSheet
} from 'react-native';
import { Actions } from 'react-native-router-flux';

// Constants && Functions
import { color } from '../common/constants';

class Schedule extends Component{
    
    constructor(props) {
        super(props)
    
        this._renderItems = this._renderItems.bind();
    }
    
    /*O _renderItems retorna a forma que os elementos da FlatList serão mostrados, neste caso
    são mostrados os elementos id, título, status e prioridade do elemento, todos como botões, 
    que mostra mais detalhes da chamada desejada*/
    _renderItems({item, index}){
        const statusValue = ['Novo', 'Process. (Atrib)', 'Process. (Plan)', 'Pendente', 'Solucionado', 'Fechado'];
        const priorityValue = ['Muito baixa', 'Baixa', 'Média', 'Alta', 'Muito Alta', 'Crítica'];

        let pColor = [
          '#ffbbbb',
          '#ffaaaa',
          '#ff9999',
          '#ff7777',
          '#ff5555',
          '#ff3333',
        ];

        const displayItem = (
            <TouchableOpacity
                style={[schStyle.elementsContainer, {backgroundColor: item.id % 2 == 0 ? color.LIGHT_GRAY : color.WHITE_GRAY}]}
                onPress={() => Actions.calldetails({item: item.id})}
            >
                <Text>{item.id}</Text>
                <Text>{item.name}</Text>
                <Text style={{color: color.PRIMARY}}>{statusValue[item.status - 1]}</Text>
                <Text style={{color: pColor[item.priority - 1]}}>{priorityValue[item.priority - 1]}</Text>
            </TouchableOpacity>
        );

        if (index == 0){
            return(
                <View>
                    <View style={schStyle.description}>
                        <Text style={schStyle.descText}>ID</Text>
                        <Text style={schStyle.descText}>Título</Text>
                        <Text style={schStyle.descText}>Status</Text>
                        <Text style={schStyle.descText}>Priodidade</Text>
                    </View>
                    {displayItem}
                </View>
            );
        }
        else{
            return displayItem;
        }
    }

    render(){
        const { list, buttonAction } = this.props;

        return (
            <View style={schStyle.container}>
                <View>
                    <FlatList
                        style={schStyle.list}
                        keyExtractor={(item, index) =>  `${index}`}
                        data={list}
                        extraData={this.props}
                        renderItem={this._renderItems}
                    />
                </View>
            </View>
        );
    }
}

export default Schedule;

const schStyle = StyleSheet.create({
  container: {
    flex: 1,
    margin: 10
  },
  description: {
    backgroundColor: color.DARK_GRAY2,
    flexDirection: 'row', 
    justifyContent: 'space-between', 
    alignItems: 'center',
    padding: 10,
    borderTopLeftRadius: 3,
    borderTopRightRadius: 3
  },
  descText: {
    color: color.PRIMARY, 
    fontSize: 16, 
    padding: 15
  },
  list: {
    flex: 4,
    backgroundColor: color.WHITE,
    borderBottomLeftRadius: 3,
    borderBottomRightRadius: 3
  },
  elementsContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: 10
  }
});