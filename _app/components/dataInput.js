/*Este Componente cria um campo personalizado de entrada de dados*/
import React, { Component } from 'react';
import { Text, TextInput, View } from 'react-native';

import { styles } from '../../_assets/styles';

export class DataInput extends Component {
  render(){
    /*A constante armazena os dados enviados ao componente quando chamados no render
    para serem tratados aqui, isso evita que sempre que sejam chamados, seja necessário 
    escrever this.props na frente
    ================================================
    ex.: inputName ficaria this.props.inputName
    */
    const { inputName, value, onChangeDataText, placeHolder, capitalization,
            keyboardChange, returnKeyType, secureTextEntry
          } = this.props;

    let capType = (capitalization) ? capitalization : 'none';
    let keyType = (keyboardChange) ? keyboardChange : 'default';
    let secure = (secureTextEntry) ? true : false;

    if (returnKeyType)
      returnKey = returnKeyType;

    return(
      <View style={{borderColor: '#eee', borderRadius: 5, backgroundColor: '#eee', marginTop: 5, marginBottom: 5}}>
        <TextInput
          style = {[styles.input, {fontSize: 14}]}
          value = {value}
          autoCapitalize = {capType}
          keyboardType = { keyType }
          onChangeText = {onChangeDataText}
          secureTextEntry = { secure }
          placeholder = {placeHolder}
          placeholderTextColor = '#888'
          underlineColorAndroid = 'transparent'
        />
      </View>
      
    );
  }
}
