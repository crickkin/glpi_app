import React, { Component } from 'react';
import { 
    View,
    Text,
    StyleSheet
} from 'react-native';

import { color } from '../common/constants';

class CallDetailItems extends Component {
    render(){
        const { name, item } = this.props;
        return (
            <View style={{flexDirection: 'row', alignItems: 'flex-start', justifyContent: 'flex-start', alignContent: 'flex-start', margin: 5}}>
                <Text style={{fontSize: 16, padding: 2}}>{name}:</Text>
                <View style={{marginLeft: 5, padding: 5, backgroundColor: color.WHITE, borderRadius: 5, elevation: 2}}>
                    <Text style={{color: color.PRIMARY, maxWidth: 220}}>
                        {item ? item : '-'}
                    </Text>
                </View>
            </View>
        );
    }
}

export default CallDetailItems;