/*Este Componente tem a função de mostrar uma barra de navegação que pode ter
uma, duas ou nenhuma ação de botão*/
import React, { Component } from 'react';
import { 
  View,
  Text, 
  TouchableOpacity
} from 'react-native';

// import Icon from 'react-native-vector-icons';

import { color } from '../common/constants';

class NavBar extends Component {
  render(){
    const { name, rightButton, rbName, leftButton } = this.props;

    return (
      <View style={{flexDirection: 'row', width: '100%', backgroundColor: color.PRIMARY, padding: 15, justifyContent: 'space-between'}}>
        <Text style={{ fontSize: 18, color: '#eee', marginLeft: rightButton != null ? 115 : 0, marginRight: leftButton != null ? 115 : 0}}>{name}</Text>
        {
          rightButton != null && (
            <TouchableOpacity onPress={() => {rightButton()}} style={{borderRadius: 4, backgroundColor: color.BLUE, paddingVertical: 8, paddingHorizontal: 15, elevation: 1}}>
              <Text>
                {rbName}
              </Text>
            </TouchableOpacity>
          )
        }
      </View>
    );
  }
}

export default NavBar;