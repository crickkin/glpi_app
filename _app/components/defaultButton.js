/*Este Componente cria um botão padrão com um nome e uma função para execução*/
import React, { Component } from 'react'
import { 
    View,
    Text,
    TouchableOpacity
} from 'react-native';

import { styles } from '../../_assets/styles';

class DefaultButton extends Component{
    render(){
        const { onPressButton, name, style } = this.props;

        return(
            <View style={style}>
                <TouchableOpacity
                    onPress={onPressButton}
                    style={styles.loginButtonContainer}
                >
                    <Text>{name}</Text>
                </TouchableOpacity>
            </View>
        );
    }
}

export default DefaultButton;