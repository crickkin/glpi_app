import React, { Component } from 'react'
import { 
    View,
    Text,
    TouchableOpacity,
    StyleSheet
} from 'react-native';

//Contants
import { color } from '../common/constants';

class RoundButton extends Component {
    render(){
        const { onPressButton } = this.props;

        return(
            <View>
                <TouchableOpacity
                    style={rbStyle.container}
                    onPress={onPressButton}
                >
                    <Text>Add</Text>
                </TouchableOpacity>
            </View>
        );
    }
}

export default RoundButton;

const rbStyle = StyleSheet.create({
    container: {
        margin: 2,
        width: 50,
        height: 50,
        borderRadius: 25,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: color.YELLOW
    }
})