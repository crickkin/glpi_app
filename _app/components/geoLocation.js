/*Este Componente tem a função de criar um novo registro de localização no GLPI*/
import React, { Component } from 'react';
import { 
    View,
    Text,
    TextInput,
    TouchableOpacity,
    StyleSheet
} from 'react-native';
//Redux Stuff
import { connect } from 'react-redux';

//Components
import DataInput from '../components/dataInput';

//Functions & Constants
import { color, API_REST } from '../common/constants/';

//Components
import DefaultButton from './defaultButton';

//Constants & Functions
import { getAllItems } from '../common/functions/Glpi';

class GeoLocation extends Component {
  constructor(props) {
    super(props);

    this.state = {
      locationName: '',
      location: {
        latitude: null,
        longitude: null
      },
      completed: false
    };

    this.addLocation = this.addLocation.bind(this);
    this.getAllLocations = this.getAllLocations.bind(this);
  }

  componentDidMount = () => {
    navigator.geolocation.getCurrentPosition(this.geoSuccess, this.geoFailure);
  };

  geoSuccess = position => {
    this.setState({ location: position.coords });
  };

  geoFailure = err => {
    console.log(err);
  };

  /*Adicionando Nova Localização ao Banco de dados*/
  addLocation() {
    const url = API_REST + 'location/';

    fetch(url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Session-Token': this.props.sessionToken
      },
      body: JSON.stringify({
        input: {
          name: this.state.locationName,
          latitude: this.state.location.latitude,
          longitude: this.state.location.longitude
        }
      })
    })
      .then(r => {
        // this.getAllLocations();
        this.setState({completed: true});
        this.props.callback();
      })
      .catch(err => {
        console.log('Error!');
        console.log(err);
      });
  }

  getAllLocations = () => {
    getAllItems('location/', this.props.sessionToken, locations => {
      console.log(locations);
    });
  };

  render() {

    const { callback } = this.props;

    return (
      <View style={{ justifyContent: 'center' }}>
        {
            !this.state.completed && (
            <View>
                <Text>Nome:</Text>
                <TextInput
                    style={{ backgroundColor: color.WHITE, borderRadius: 5, height: 40 }}
                    value={this.state.locationName}
                    onChangeText={locationName => {this.setState({ locationName });}}
                    underlineColorAndroid="transparent"
                />
                {this.state.location.latitude != null && (<Text>Latitude: {this.state.location.latitude}</Text>)}
                {this.state.location.longitude != null && (<Text>Longitude: {this.state.location.longitude}</Text>)}
                <DefaultButton name="Confirmar" onPressButton={this.addLocation}/>
            </View>
        )}
        {
            this.state.completed && (
                <View>
                    <Text>{this.state.locationName} adicionado.</Text>
                </View>
            )
        }
      </View>
    );
  }
}

const mapStateToProps = state => {
    return {    sessionToken: state.sessionToken.sessionToken   };
}

export default connect(mapStateToProps)(GeoLocation);