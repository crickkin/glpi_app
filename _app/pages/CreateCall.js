/*Esta página permitirá que o usuário preencha as informações para criar 
Uma nova Chamada no GLPI*/
import React, { Component } from 'react'
import { 
    View,
    ScrollView,
    Text,
    KeyboardAvoidingView,
    Picker,
    StyleSheet
} from 'react-native';
import { Actions } from 'react-native-router-flux';

/*Redux Stuff*/
import { connect } from 'react-redux';

/*Components*/
import NavBar from '../components/navbar';
import { DataInput } from '../components/dataInput';
import DefaultButton from '../components/defaultButton';
import GeoLocation from '../components/geoLocation';

//Contansts && Functions
import { color, API_REST } from '../common/constants';
import { getAllItems } from '../common/functions/Glpi';

class CreateCall extends Component {
  constructor(props) {
    super(props);

    this.state = {
      name: '',
      content: '',
      status: 2,
      priority: 3,
      locations_id: 0,
      locations: []
    };

    this.options = {
      status: [
        { label: 'Novo', value: 1  },
        { label: 'Processando (atribuído)', value: 2  },
        { label: 'Processando (planejado)', value: 3  },
        { label: 'Pendente', value: 4  },
        { label: 'Solucionado', value: 5  },
        { label: 'Fechado', value: 6  }
      ],
      priority: [
        { label: 'Crítica', value: 6  },
        { label: 'Muito Alta', value: 5 },
        { label: 'Alta', value: 4 },
        { label: 'Média', value: 3  },
        { label: 'Baixa', value: 2  },
        { label: 'Muito Baixa', value: 1  }
      ],
    }

    this.addCall = this.addCall.bind(this);
    this.getAllLocations = this.getAllLocations.bind(this);
  }

  addCall() {
    const url = API_REST + 'ticket/';

    fetch(url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Session-Token': this.props.sessionToken
      },
      body: JSON.stringify({
        input: {
          name: this.state.name,
          content: this.state.content,
          status: this.state.status,
          priority: this.state.priority,
          locations_id: this.state.locations_id
        }
      })
    })
      .then(r => {
        Actions.callview();
      })
      .catch(err => {
        alert(err);
      });
  }

  getAllLocations() {
    getAllItems('location/', this.props.sessionToken, l => {
      this.setState({locations: l});
      console.log(this.state.locations);
    });
  }

  componentWillMount() {
    this.getAllLocations();
  }

  render() {
    return (
      <View style={{  flex: 1,justifyContent: 'center', backgroundColor: color.WHITE_GRAY }}>
        <NavBar name="Nova Chamada" />
        <ScrollView style={{ flex: 1, padding: 20, margin: 10 }}>
          <KeyboardAvoidingView behaviour="padding" enabled>
            <Text style={crclStyles.itemTitle}>Título:</Text>
            <DataInput
              value={this.state.name}
              onChangeDataText={name => this.setState({ name })}
              placeHolder="Título"
              placeholderTextColor="#888"
              underlineColorAndroid="transparent"
            />
            <Text style={crclStyles.itemTitle}>Descrição:</Text>
            <DataInput
              value={this.state.content}
              onChangeDataText={content => this.setState({ content })}
              placeHolder="Descrição"
              placeholderTextColor="#888"
              underlineColorAndroid="transparent"
            />
            <Text style={crclStyles.itemTitle}>Status:</Text>
            <View style={crclStyles.pickerContainer}>
              <Picker
                selectedValue={this.state.status}
                style={{ color: color.DARK_GRAY }}
                onValueChange={ (itemValue, itemIndex) =>  this.setState({ status: itemValue }) }
              >
                {this.options.status.map(item => {
                  return (
                    <Picker.Item key={item.value} label={item.label} value={item.value} />
                  );
                })}
              </Picker>
            </View>
            <Text style={crclStyles.itemTitle}>Prioridade:</Text>
            <View style={crclStyles.pickerContainer}>
              <Picker
                selectedValue={this.state.priority}
                style={{ color: color.DARK_GRAY }}
                onValueChange={(itemValue, itemIndex) => this.setState({ priority: itemValue })  }
              >
                {this.options.priority.map(item => {
                  return (
                    <Picker.Item key={item.value} label={item.label} value={item.value} />
                  );
                })}
              </Picker>
            </View>
            <Text style={crclStyles.itemTitle}>Localização</Text>
            <View style={crclStyles.pickerContainer}>
              <Picker
                selectedValue={this.state.locations_id}
                style={{  color: color.DARK_GRAY  }}
                onValueChange={(itemValue, itemIndex) => this.setState({  locations_id: itemValue })}
              >
                {this.state.locations.map(item => {
                  return (
                    <Picker.Item key={item.id} label={item.name} value={item.id}/>
                  );
                })}
                <Picker.Item label='Criar Novo...' value={this.state.locations.lenght}/>
              </Picker>
            </View>
            { 
              this.state.locations_id == this.state.locations.lenght 
              && (<GeoLocation callback={() => {
                this.getAllLocations()
                console.log("Testando callback")
                }}/>)  
            }
          </KeyboardAvoidingView>
          <View style={{ alignItems: 'center' }}>
            <DefaultButton name="Criar" onPressButton={this.addCall} />
          </View>
        </ScrollView>
      </View>
    );
  }
}

const mapStateToProps = state => {
  return { sessionToken: state.sessionToken.sessionToken };
}

export default connect(mapStateToProps)(CreateCall);

const crclStyles = StyleSheet.create({
  pickerContainer: {
    backgroundColor: color.WHITE,
    borderRadius: 5,
    marginTop: 5,
    marginBottom: 5
  },
  itemTitle: { 
    fontSize: 16
  }
});