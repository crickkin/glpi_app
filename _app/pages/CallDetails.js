/*Esta Classe tem a função de mostrar detalhadamente as informações de uma chamada.
Utilizando a API para o item com o ID equivalente ao que lhe é passado através do 
Router Flux no momento que ocorre a transição da classe Call View para esta.*/
import React, { Component } from 'react'
import { 
    View,
    Text,
    ScrollView,
    StyleSheet
} from 'react-native';

//Redux Stuff
import { connect } from 'react-redux';

//Components
import NavBar from '../components/navbar';
import DefaultButton from '../components/defaultButton';
import CallDetailItems from '../components/callDetailItems';

//Constants 'n Functions
import { color } from '../common/constants';
import { getAllItems, getItem } from '../common/functions/Glpi';

class CallDetails extends Component{
    
    constructor(props) {
      super(props)
    
      this.state = {
         call: {
             name: '',
             date_creation: '',
         },
         location: {},
      };

      this.getTicket = this.getTicket.bind(this);
      this.getLocation = this.getLocation.bind(this);
    };
    
    getTicket()
    {
        getItem('ticket', this.props.item, this.props.sessionToken, c => {
            this.setState({ call: c });
            console.log(this.state.call);
            this.getLocation();
          }
        );
    }

    getLocation() {
        getAllItems('location', this.props.sessionToken, l => {
          this.setState({
            location: l.filter(item => {
              if (item.id == this.state.call.locations_id) return item;
            })[0]
          });
        });
    }

    componentWillMount() {
        this.getTicket();
    }
    

    render(){
        const { item } = this.props;
        const statusValue = ['Novo', 'Processando (Atribuído)', 'Processando (Planejado)', 'Pendente', 'Solucionado', 'Fechado'];
        const priorityValue = ['Muito baixa', 'Baixa', 'Média', 'Alta', 'Muito Alta', 'Crítica'];

        return(
            <View style={{flex: 1}}>
                <NavBar
                    name={this.state.call.name}
                />
                <ScrollView>
                    <CallDetailItems
                        name = 'Data de Abertura'
                        item = {this.state.call.date_creation}
                    />
                    <CallDetailItems
                        name = 'Última atualização'
                        item = {this.state.call.date_mod}
                    />
                    <CallDetailItems
                        name = 'Tempo para aceitar'
                        item = {this.state.call.time_to_own}
                    />
                    <CallDetailItems
                        name = 'Tempo interno para possuir'
                        item = {this.state.call.internal_time_to_own}
                    />
                    <CallDetailItems
                        name = 'Tempo para solucionar'
                        item = {this.state.call.time_to_resolve}
                    />
                    <CallDetailItems
                        name = 'Tempo interno para solução'
                        item = {this.state.call.internal_time_to_resolve}
                    />
                    <CallDetailItems
                        name = 'Tipo'
                        item = { (this.state.call.type == 1) ? 'Incidente' : 'Requisição'}
                    />
                    <CallDetailItems
                        name = 'Status'
                        item = {statusValue[this.state.call.status - 1]}
                    />
                    <CallDetailItems
                        name = 'Urgência'
                        item = {priorityValue[this.state.call.urgency - 1]}
                    />
                    <CallDetailItems
                        name = 'Impacto'
                        item = {priorityValue[this.state.call.impact - 1]}
                    />
                    <CallDetailItems
                        name = 'Prioridade'
                        item = {priorityValue[this.state.call.priority - 1]}
                    />
                    <CallDetailItems
                        name = 'Localização'
                        item = {this.state.location ? this.state.location.name : '-'}
                    />
                    <CallDetailItems
                        name = 'Ator'
                    />
                    <CallDetailItems
                        name = 'Descrição'
                        item = {this.state.call.content}
                    />
                </ScrollView>
            </View>
        );
    }
}

const mapStateToProps = state => {
    return { sessionToken: state.sessionToken.sessionToken }
}

export default connect(mapStateToProps)(CallDetails);