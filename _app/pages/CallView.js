/*A página de CallView usa o componente schedule para mostrar a lista de chamadas
retornadas do GLPI. futuramente será criado um botão para criar chamadas diretamente
pelo app*/
import React, { Component } from 'react'
import { 
    View,
    Text,
    Stylesheet,
    TouchableOpacity
} from 'react-native';

//Redux Stuff
import { connect } from 'react-redux';

import { Actions } from 'react-native-router-flux';

//Components
import NavBar from '../components/navbar';
import Schedule from '../components/schedule';
import DefaultButton from '../components/defaultButton';
import RoundButton from '../components/roundButton';

//Constants && Functions
import { status, color, API_REST } from '../common/constants';
import { getAllItems } from '../common/functions/Glpi';
import { styles } from '../../_assets/styles';

class CallView extends Component {
    constructor(props) {
      super(props)
    
      this.state = {
        calls: [
          {
            id: '-',
            name: '-',
            status: '-',
            priority: '-'
          }
        ]
      };

      this.getCalls = this.getCalls.bind(this);
    }
    
    getCalls(){
      getAllItems('ticket/', this.props.sessionToken, (c) => {
        this.setState({ calls: c });
      });
    }

    componentDidMount = () => {
        this.getCalls();
    };
    
    render(){
        return (
          <View
            style={{
              flex: 1,
              justifyContent: 'flex-start',
              alignItems: 'center'
            }}
          >
            <NavBar name="Chamadas"
              rightButton = {() => this.getCalls()}
              rbName = 'R' 
            />
            <Schedule
              list={this.state.calls}
              buttonAction={() => console.log(`item pressed`)}
            />
            <View style={{ margin: 10 }}>
              <RoundButton onPressButton={() => Actions.createcall()} />
            </View>
          </View>
        );
    }
}

const mapStateToProps = state => {
    return { sessionToken: state.sessionToken.sessionToken }
}

export default connect(mapStateToProps)(CallView);