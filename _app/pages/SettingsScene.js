//Bolsa de Pesquisa - 'Goyta Call'
  //@developer Nícolas F. de Souza
/*Esta página será utilizada para elementos de configuração*/
import React, { Component } from 'react';
import {
  View, 
  Text
} from 'react-native';
import { Actions } from 'react-native-router-flux';

import { styles } from '../../_assets/styles';

class SettingsScene extends Component{
  render() {
    return(
      <View style = {styles.container}>
        <Text>Settings</Text>
      </View>
    );
  }
}

export default SettingsScene;