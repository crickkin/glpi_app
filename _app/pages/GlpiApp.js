//Bolsa de Pesquisa - 'Glpi App'
  //@developer Nícolas F. de Souza
/*A página GLPI App serve como página central que gerencia a transição entre páginas através
da biblioteca Router Flux, que divide as páginas em cenas que possuem um identficador denominado key
que pode ser utilizado com o comando Action.nome_da_key() para transitar de uma cena a outra*/
import React, {Component} from 'react';
import { Router, Scene } from 'react-native-router-flux';

/*Páginas*/
import LoginScene from './LoginScene';
import MainScene from './MainScene';

import CallView from './CallView';
import CreateCall from './CreateCall';
import SettingsScene from './SettingsScene';
import CallDetails from './CallDetails';

class GlpiApp extends Component<Props> {
  render() {
    return (
      <Router>
        <Scene key="root">
          <Scene
            key="login"
            component={LoginScene}
            title="Login"
            hideNavBar
            initial
          />
          <Scene key="main" component={MainScene} title="Main" hideNavBar />
          <Scene
            key="callview"
            component={CallView}
            title="CallView"
            hideNavBar
          />
          <Scene
            key="createcall"
            component={CreateCall}
            title="CreateCall"
            hideNavBar
          />
          <Scene
            key="settings"
            component={SettingsScene}
            title="Settings"
            hideNavBar
          />
          <Scene
            key="calldetails"
            component={CallDetails}
            title="CallDetails"
            hideNavBar
          />
        </Scene>
      </Router>
    );
  }
}

export default GlpiApp;