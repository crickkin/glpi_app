/*Essa página faz a autênticação de login através da chamada de api do GLPI,
onde a action cuida da autenticação e do armazenamento do token de acesso, e um callback
lida com a ação tomada a partir do resultado*/
import  React, { Component } from 'react';
import { 
  View, 
  Text, 
  TextInput, 
  TouchableOpacity, 
  Image, 
  KeyboardAvoidingView 
} from 'react-native';
import { Actions } from 'react-native-router-flux';
//Redux Stuff
import { connect } from 'react-redux';
import { initSession } from '../actions';

//Components
import { DataInput } from '../components/dataInput';
import DefaultButton from '../components/defaultButton';

//Constants && Functions
import base64 from 'react-native-base64';
import { color } from '../common/constants/';

import { styles } from '../../_assets/styles';

class LoginScene extends Component<Props> {
  constructor(){
    super();

    this.state = {
      username: '',
      password: '',
      error: ''
    };

    this.login = this.login.bind(this);
  }

  login(){
    const { username, password } = this.state;

    this.props.initSession(username, password, (st) => {
      if (st == undefined){
        alert('Usuário ou Senha inválida');
      }
      else{
        Actions.main();
      }
    });
  }

  render(){
    return (
      <View style = {styles.loginContainer}>
        <View style = {styles.loginBox}>
          <View style = {{margin: 10}}>
            <KeyboardAvoidingView behavior = 'padding' enabled>
              <DataInput
                  value = {this.state.username}
                  onChangeDataText = {username => this.setState({username})}
                  placeholder = 'Usuário'
                  placeholderTextColor = '#888'
                  underlineColorAndroid = 'transparent'
              />
              <DataInput
                value = {this.state.password}
                onChangeDataText = {password => this.setState({password})}
                placeholder = 'Senha'
                secureTextEntry = { true }
                placeholderTextColor = '#888'
                underlineColorAndroid = 'transparent'
              />
            </KeyboardAvoidingView>
          </View>
          <DefaultButton
            name='Login'
            style={{alignItems: 'center'}}
            onPressButton={this.login}
          />
        </View>
      </View>
    );
  }
}

mapStateToProps = state => {
  return {  sessionToken: state.sessionToken.sessionToken  };
}

export default connect(mapStateToProps, { initSession })(LoginScene);