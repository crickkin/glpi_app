//Bolsa de Pesquisa - 'Glpi App'
  //@developer Nícolas F. de Souza
/*A MainScene é carregada logo após a autenticação de login, mostrando as opções 
de funcionalidade que o usuário possui.*/
import React, { Component } from 'react';
import { 
  Platform, 
  Text, 
  View,
  TouchableOpacity
} from 'react-native';
import ScrollableTabView, { DefaultTabBar } from 'react-native-scrollable-tab-view';
import { Actions } from 'react-native-router-flux';

//Redux Stuff
import { connect } from 'react-redux';

//Components
import { NavBar } from '../components/navbar';
import GeoLocation from '../components/geoLocation';

//Constants && Functions
import { color } from '../common/constants';
import { styles } from '../../_assets/styles';

class MainScene extends Component<Props> {
  render() {
    return (
        <ScrollableTabView
          tabBarBackgroundColor = {color.PRIMARY}
          tabBarActiveTextColor = {'#eee'}
          tabBarUnderlineStyle = {{backgroundColor: '#eee'}}
          tabBarPosition = {'top'}
          renderTabBar = {() => <DefaultTabBar />}
        >
          <View tabLabel='Gerênciamento de Chamadas' style={{flex:1, justifyContent: 'center', alignItems: 'center'}}>
            <View style={{backgroundColor: color.PRIMARY, padding: 15, borderRadius: 5}}>
              <TouchableOpacity onPress={() => {  Actions.callview()  }}>
                <Text style={{color: 'white', fontSize: 20}}>Chamadas</Text>
              </TouchableOpacity>
            </View>
          </View>
        </ScrollableTabView>
    );
  }
}

const mapStateToProps = state => {
  return {  sessionToken: state.sessionToken.sessionToken }
}

export default connect(mapStateToProps)(MainScene);