/*GLPI Comands*/
/*Um documento contendo as funções de chamada esseciais da api, atualmente está sendo utilizada
apenas como código de referência*/
import * as c from '../constants';
import base64 from 'react-native-base64';

/*Para iniciar a sessão é necessário utilizar esse procedimento para resgatar
um token de sessão.*/
export const initSession = (login, password) => {
  var text = login + ':' + password;
  const authLogin = base64.encode(text);

  fetch(c.API_REST + 'initSession/', {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': 'Basic ' + authLogin,
    }
  })
  .then((response) => response.json())
  .then((s) => {
    alert(s.session_token);
  })
  .catch((err) => {
    alert(err);
  });
}

/*Retorna todos os itens de um tipo.
Ex.: Mostrar todas as chamadas existentes*/
export function getAllItems(type, sessionToken, callback){
  const url = c.API_REST + type;

      fetch(url, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          'Session-Token': sessionToken
        }
      })
      .then(response => response.json())
      .then(c => {
        callback(c);
      })
      .catch(err => {
        alert(err);
      });
}


/*Retorna um item específico de deteerminado tipo
Ex.: Retornar localização com id 1*/
export const getItem = (type, id, sessionToken, callback) => {
  const url = c.API_REST + type + '/' + id;

  fetch(url, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
      'Session-Token': sessionToken
    }
  })
    .then(response => response.json())
    .then(r => {
      callback(r);
    })
    .catch(err => {
      alert(err);
    });  
}

export const addItem = (itemType, sessionToken) => {
  fetch(API_REST + itemType, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Session-Token': sessionToken,
    }
  });
}
