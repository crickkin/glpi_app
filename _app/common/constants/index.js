/*
=======================
*********API's*********
=======================
*/
/**======GLPI=======**/
/*O api rest fará a chamada as funções específicas do Glpi 
para recuperar os dados salvos nele para gerenciamento*/
export const API_REST = 'http://10.80.0.53/glpi/apirest.php/';

/*========ACTIONS======*/
/*As Actions servem para enviar uma solicitação ao redux para salvar informações 
que precisam ser compartilhadas pela aplicação*/
//SESSION TOKEN
export const GET_SESSION_TOKEN = 'GET_SESSION_TOKEN';
export const REMOVE_SESSION_TOKEN = 'REMOVE_SESSION_TOKEN';

/**=====GEOCODING=====**/
// export const GEO_API_KEY = 'AIzaSyBCEj8MDQA2-P69Vt7KXTQpMhg4vB0UpbM';

/*========MICELANIUS========*/
//DIRECTORIES
export const directory = {
  IMAGES: '../../_assets/images/',
  STYLES: '../../_assets/styles/',
}

//COLORS
export const color = {
  PRIMARY: '#116b9d',
  BLUE: '#227cae',
  WHITE: '#eee',
  DARK_BLUE: '#0e547c',
  WHITE_GRAY: '#ccc',
  LIGHT_GRAY: '#bbb',
  DARK_GRAY: '#888',
  DARK_GRAY2: '#9f9f9f',
  DARKER_GRAY: '#444',
  YELLOW: '#f9b200',
  DARK_YELLOW: '#734c02',
}

/*Defini as opções de status para a chamada da aplicação*/
export const status = {
  PROCESSING_ATTRIBUTTED: 'PROCESSING_ATTRIBUTTED',
  PROCESSING_PLANNING: 'PROCESSING_PLANNING',
  PENDING: 'PENDING',
  SOUVED: 'SOUVED',
  CLOSED: 'CLOSED'
}