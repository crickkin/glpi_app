React Native Documantation:
```
https://facebook.github.io/react-native/docs/getting-started.html
```

GLPI API Rest Documentation:
```
https://github.com/glpi-project/glpi/blob/9.1/bugfixes/apirest.md
```

Para alterar servidor glpi utilizado pela aplicação basta acessar ``/_app/common/constants/index.js`` e mudar a url
da contante API_REST.