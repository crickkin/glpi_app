//Bolsa de Pesquisa - 'Goyta Call'
  //@developer Nícolas F. de Souza
import React, {Component} from 'react';

import { Provider } from 'react-redux';
import { store } from './_app/store';

import GlpiApp from './_app/pages/GlpiApp';

export default class App extends Component<Props> {
  render() {
    return (
        <Provider store = { store }>
            <GlpiApp />
        </Provider>
    );
  }
}
