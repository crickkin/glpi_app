//_assets/styles/index.js
/*Este Componente tem a função de criar uma constante com estilos utilizados por todo o app*/
import React, { Component } from 'react';
import { StyleSheet } from 'react-native';

import { color } from '../../_app/common/constants';
import { dimensions } from './base';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-start',
  },
  rowContainer: {
      flex: 1,
      flexDirection: 'row',
      justifyContent: 'space-between',
      margin: 60,
  },
  navBar: {
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
    backgroundColor: color.PRIMARY,
    paddingBottom: 20,
  },
  navText: {
    color: 'white',
    marginLeft: 30,
    marginRight: 30,
    marginTop: 15,
    fontSize: 20,
  },
  buttonContainer: {
    borderWidth: 0.01,
    borderRadius: 20,
    backgroundColor: color.PRIMARY,
    borderColor: color.PRIMARY,
    width: 96,
    height: 96,
  },
  buttonSubmitContainer: {
    borderWidth: 0.01,
    borderRadius: 20,
    backgroundColor: color.PRIMARY,
    borderColor: color.PRIMARY,
    width: 96,
    height: 64,
  },
  submitFont: {
    marginTop: 15,
    marginLeft: 20,
    fontSize: 20,
    color: 'white',
  },
  buttonImage: {
    margin: 8,
    width: 76,
    height: 76,
  },
  input: {
      marginTop: 5,
      alignItems: 'center',
      paddingBottom: 8,
      borderColor: color.LIGHT_GRAY,
      fontSize: 18,
      color: color.DARK_GRAY,
  },
  loginContainer: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: color.PRIMARY,
  },
  loginBox: {
    margin: 30,
    padding: 20,
    borderRadius: 10
  },
  loginButton: {
    marginTop: 200,
    marginBottom: 50,
    marginLeft: 320,
    borderWidth: 0.01,
    borderColor: '#eee',
    backgroundColor: '#eee',
    borderRadius: 50,
    width: 60,
    height: 60,
    justifyContent: 'center',
  },
  loginButtonContainer: {
    marginTop: 12,
    marginLeft: 5,
    marginRight: 5,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 0.01,
    borderRadius: 15,
    backgroundColor: color.YELLOW,
    borderColor: color.YELLOW,
    width: 120,
    height: 42,
  },
});
